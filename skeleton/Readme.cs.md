# Vítejte ve vašem novém kolektivu

**Organizujte se a vytvářejte sdílené znalosti!**


### 👥 Pozvěte do kolektivu nové lidi

Ať už chcete přidat své přátele či celé skupiny, jděte do [aplikace Okruhy](/index.php/apps/circles/) a přidejte nové členy.

### 🌱 Vdechněte svému kolektivu život

Vytvářejte stránky a sdílejte své myšlenky, které jsou opravdu důležité. Ať už se jedná o repozitář sdílených znalostí pro komunitu nebo příručka pro vaši organizaci – Kolektivy fungují!

### 🛋️ Upravte si tuto úvodní stránku tak, aby vám vyhovovala

Klikněte na tlačítko tužky a začněte! ↗️


## Také dobré vědět

* Odkazujte na lokální stránky vybráním textu a zvolením „odkázat na soubor“.
* Stejnou stránku může upravovat vícero lidí naráz.
* Další informace o této aplikaci naleznete v [dokumentaci](https://collectivecloud.gitlab.io/collectives/).
* Pokud máte dotazy, obraťte se o pomoc na [komunitu](https://help.nextcloud.com/c/apps/collectives/174).
