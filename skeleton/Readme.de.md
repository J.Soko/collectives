# Willkommen zu deinem neuen Kollektiv

**Mache mit, organisiere und baue gemeinsames Wissen auf!**


### 👥 Lade weitere Personen ins Kollektiv ein

Wenn du deine Freund:innen oder ganze Gruppen hinzufügen möchtest, klicke auf den Link "Mitglieder verwalten", den du im Aktionsmenü in der Kollektivliste links findest.

### 🌱 Bringe Leben in dein Kollektiv

Erstelle Seiten und teile deine wertvollen Gedanken. Ob eine gemeinsame Wissensdatenbank für die Community oder ein Handbuch für deine Organisation – Kollektive funktioniert!

### 🛋️ Bearbeite diese Startseite, um dich wie zu Hause zu fühlen

Klicke auf den Stift um los zulegen! ↗️


## Auch gut zu wissen

* Dieselbe Seite kann von mehreren Personen gemeinsam bearbeitet werden.
* Verlinke lokale Seiten, indem du Text auswählst und "Datei verknüpfen" wählst. Drag & Drop aus der Seitenliste in den Editor funktioniert ebenfalls.
* Erstelle Vorlagen für künftige Seiten, indem eine "Template" Seite erstellst.
* Erfahre in der [Dokumentation](https://collectivecloud.gitlab.io/collectives/) mehr über diese App.
* Frage [im Forum](https://help.nextcloud.com/c/apps/collectives/174) um Hilfe, oder falls du eine Frage hast.
